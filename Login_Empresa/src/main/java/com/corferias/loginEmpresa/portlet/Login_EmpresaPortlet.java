package com.corferias.loginEmpresa.portlet;

import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.auth.session.AuthenticatedSessionManagerUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.util.PropsUtil;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author LENOVO
 */
@Component(
	immediate = true,
	property = {
			"com.liferay.portlet.display-category=category.sample",
			"com.liferay.portlet.header-portlet-css=/css/main.css",
			"com.liferay.portlet.instanceable=false",
			"javax.portlet.display-name=Login_Empresa",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.view-template=/view.jsp",
			"javax.portlet.name=Login_EmpresaPortlet",
			"javax.portlet.resource-bundle=content.Language",
			"javax.portlet.security-role-ref=power-user,user"	
	},
	service = Portlet.class
)
public class Login_EmpresaPortlet extends MVCPortlet {
	
	public void logearEmpresa(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
	
	ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
		WebKeys.THEME_DISPLAY);
	
	HttpServletRequest request = PortalUtil.getOriginalServletRequest(
		PortalUtil.getHttpServletRequest(actionRequest));
	
	HttpServletResponse response = PortalUtil.getHttpServletResponse(
		actionResponse);

	String nit = ParamUtil.getString(actionRequest, "nit");
	System.out.println("nit: "+ nit);
	
	String login = ParamUtil.getString(actionRequest, "login");
	String password = ParamUtil.getString(actionRequest, "password");
	//String password = actionRequest.getParameter("password");
	boolean rememberMe = ParamUtil.getBoolean(actionRequest, "rememberMe");
	String authType = CompanyConstants.AUTH_TYPE_EA;
	
	if(ClienteRest.validarEmpresa(nit)) {
		
	try {
		AuthenticatedSessionManagerUtil.login(
			request, response, login, password, rememberMe, authType);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println("Error al Autenticar al usuario");
		e.printStackTrace();
		SessionErrors.add(request, "errorUsuario");
	}
	
	//System.out.println("themeDisplay.getPathMain() :"+themeDisplay.getPathMain());
	
	//String paginaEmpresas = PropsUtil.get("home.empresa");
	//actionResponse.setRenderParameter("mvcPath", "/META-INF/resources/userDetail.jsp");
	
	actionResponse.sendRedirect(PropsUtil.get("home.empresa"));
	
	}
	
	else {
		
		SessionErrors.add(request, "errorLogueo");
		//actionResponse.sendRedirect(PropsUtil.get("home"));
	}
	
}
}