package com.corferias.loginEmpresa.portlet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class ClienteRest {
	
	public static boolean validarEmpresa(String nit){

		boolean respuesta = false;
		
        try {
        	
        	URL url = new URL("https://esb-acc.corferias.co/services/sucesosService/SucesoREST");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                        
            String output;
            System.out.println("Output from Server .... \n");
            StringBuilder sb = new StringBuilder();
      
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
            
            org.json.JSONObject object = new org.json.JSONObject(sb.toString());
            
            org.json.JSONObject Jobject2 = object.getJSONObject("sucesos");
            
            System.out.println(object);  
            System.out.println(Jobject2);  
            
            org.json.JSONArray array = Jobject2.getJSONArray("suceso");
            
            for(int i=0; i < array.length(); i++)   
            {  
	            System.out.println(array.getJSONObject(i).getString("IdEvento"));
	            
	            if(array.getJSONObject(i).getString("IdEvento").equalsIgnoreCase(nit)) {
	            	return true;
	            }
	            
            }  
     
            conn.disconnect();
                        
            respuesta = false;
            
        } catch (Exception e) {
    		System.out.println("Error al consumir el servicio rest"+ e);
    		respuesta = false;
        }
        
        return respuesta;
	}

}
